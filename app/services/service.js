/* global Instructor */
var Instructor;
(function(Instructor){
    Instructor.Service = (function(){
        
                                function Service($http,$q,REST_API){
                                    this.$http = $http;
                                    this.$q = $q;
                                    this.REST_API = REST_API;
                                }
                                
                                
                                Service.prototype.setToken =  function(obj){
                                    
                                    var deferred = this.$q.defer();
                                    
                                    if(this.getToken() != "string"){
                                        this.$http({  method: "POST",
                                                  url: this.REST_API.getToken,
                                                  cache:false
                                               }).success(function (response, status) {           
                                                                                                                                                                                     
											                                                    window.sessionStorage.setItem("token",response.token)
											                                                    deferred.resolve(response.token);
											                                            }).error(function (response, status) {                 
                                                                                                alert("Unable to get Token")                                                                           
                                                                                                 deferred.reject("An error occured while fetching token");   					   
											                                            });
                                    }else{
                                        deferred.resolve(this.getToken());
                                    }
                                    
                                    
                                    
                                    return deferred.promise;
                                }
                                
                                
                                Service.prototype.getToken = function(){
                                   return window.sessionStorage.getItem("token");
                                }
                                
                                
                                Service.prototype.getCourses = function(page){
                                    var deferred = this.$q.defer();
                                     this.$http({  method: "GET",
                                                  url: this.REST_API.getCourses + "?per_page=2&page=" + page,
                                                  cache:false,
                                                  headers:{
                                                      Authorization: this.getToken()
                                                  }
                                               }).success(function (response, status) {           
                                                                                                                                                                                     
											                                                    
											                                                    deferred.resolve(response);
											                                            }).error(function (response, status) {                 
                                                                                                alert("Unable to get Courses")                                                                           
                                                                                                 deferred.reject("An error occured while fetching Cources");   					   
											                                            });
                                                                                        
                                    return deferred.promise;
                                }
                                
                                Service.prototype.getCourse = function(id){
                                    var deferred = this.$q.defer();
                                     this.$http({  method: "GET",
                                                  url: this.REST_API.getCourses + "/" + id,
                                                  cache:false,
                                                  headers:{
                                                      Authorization: this.getToken()
                                                  }
                                               }).success(function (response, status) {           
                                                                                                                                                                                     
											                                                    
											                                                    deferred.resolve(response);
											                                            }).error(function (response, status) {                 
                                                                                                alert("Unable to get Courses")                                                                           
                                                                                                 deferred.reject("An error occured while fetching Cources");   					   
											                                            });
                                                                                        
                                    return deferred.promise;
                                }
                                
                                
                                return Service;
        
                            })();
    
})(Instructor || (Instructor = {}))
instr.service("InstructorService",["$http","$q","REST_API",Instructor.Service])