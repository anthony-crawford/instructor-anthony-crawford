/* global Instructor */
var Instructor;
(function(Instructor){
    Instructor.Controller = (function(){
                                function Controller($scope,$routeParams,$location,InstructorService){
                                    this.$scope = $scope;
                                    this.$routeParams = $routeParams;
                                    this.$location = $location;
                                    this.InstructorService = InstructorService;
                                    this.courses=[];
                                    
                                        this.multiple =true;
                                    if(typeof $routeParams.id != 'undefined' && !isNaN($routeParams.id)){
                                         this.multiple =false;
                                        this.getCourse($routeParams.id);
                                    }else{
                                        this.getCourses();
                                    }                                    
                                    this.page = 1
                                    $scope.Instructor = this;
                                }
                                
                                Controller.prototype.redirect = function(id){
                                        this.$location.path("/"+id);
                                }
                                
                                
                                Controller.prototype.getCourses = function(direction){
                                   var that = this;
                                   switch(direction){
                                       case "forward":
                                        ++this.page;
                                       break;
                                       case "backward":
                                        --this.page;
                                       break;
                                       default:
                                       break;
                                   }
                                   this.InstructorService.getCourses(this.page).then(function(response){
                                       that.courses = response
                                   })
                                }
                                
                                Controller.prototype.getCourse = function(id){
                                   var that = this;
                                   this.InstructorService.getCourse(id).then(function(response){
                                       that.courses = response
                                   })
                                }
                                
                                return Controller;
        
                            })();
    
})(Instructor || (Instructor = {}))
instr.controller("InstructorController",["$scope","$routeParams","$location","InstructorService",Instructor.Controller])