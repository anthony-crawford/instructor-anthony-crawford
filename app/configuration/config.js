
/* global angular */
var instr = angular.module("instructor",["ngRoute","ngResource"]);

instr.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {

    $routeProvider
        .when("/", {
            title:"Instructor Courses",
            templateUrl: 'templates/index.html',
            controller: "InstructorController",
            resolve: {
                settoken                       :   function(InstructorService){ 
                                                                                    return InstructorService.setToken();
                                                                                }
            }

        })
        .when("/:id", {
            title:"Instructor Courses",
            templateUrl: 'templates/index.html',
            controller: "InstructorController",
            resolve: {
                settoken                       :   function(InstructorService){ 
                                                                                    return InstructorService.setToken();
                                                                                }
            }

        })
        .otherwise({ template: '404' });

    $locationProvider.html5Mode(false);

} ]);
instr.constant("REST_API", { 
                                "getCourses": "http://canvas-api.herokuapp.com/api/v1/courses",
                                "getToken":"http://canvas-api.herokuapp.com/api/v1/tokens"
                        }
                    );
